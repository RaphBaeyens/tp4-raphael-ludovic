﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace tp4
{
    /// <summary>
    /// Logique d'interaction pour PageDeadmau5.xaml
    /// </summary>
    public partial class PageDeadmau5 : Page
    {
        public PageDeadmau5()
        {
            //les vidéos n'ont pas rapport, je n'ai pas pu trouver de videos concernant la musique...
            InitializeComponent();
            mediaDeadmau5.Source = new Uri(@"F:/multimedia2/tp4v2/tp4/video/vid1.mp4", UriKind.Absolute);
        }

        

        private void linkDM(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("chrome.exe","https://en.wikipedia.org/wiki/Deadmau5");

        }

        private void playVid(object sender, RoutedEventArgs e)
        {
            mediaDeadmau5.Play();

        }

        private void stopVid(object sender, RoutedEventArgs e)
        {
           
            mediaDeadmau5.Pause();
        }

        private void loop(object sender, EventArgs e)
        {
            mediaDeadmau5.Position = TimeSpan.Zero;
            mediaDeadmau5.Play();
        }


    }
}
