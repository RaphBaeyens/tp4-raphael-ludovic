﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace tp4
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ArrayList array;
        private ListBox list;

     
        public MainWindow()
        {
           
            InitializeComponent();
            

            array = new ArrayList();
            array.Add("F:/multimedia2/tp4v2/tp4/images/western.jpg");
            array.Add("F:/multimedia2/tp4v2/tp4/images/dawn_of_the_dead.preview.jpg");
            array.Add("F:/multimedia2/tp4v2/tp4/images/apes.jpg");
            array.Add("F:/multimedia2/tp4v2/tp4/images/Dawn_of_the_dead2.jpg");
            array.Add("F:/multimedia2/tp4v2/tp4/images/2001.jpg");
            array.Add("F:/multimedia2/tp4v2/tp4/images/shining.jpg");

            MainWindow mw = (MainWindow)Application.Current.MainWindow;
            
            listBox.Background = new SolidColorBrush(Colors.Aqua);
            listBoxImages.Background = new SolidColorBrush(Colors.Aqua);
            listBoxChoix.Background = new SolidColorBrush(Colors.Aqua);
            mw.Background = new SolidColorBrush(Colors.Aqua);
            jeux.Background = new SolidColorBrush(Colors.Green);
            film.Background = new SolidColorBrush(Colors.Red);
            musique.Background = new SolidColorBrush(Colors.Blue);

            rect1.Visibility = Visibility.Hidden;
            cercle.Visibility = Visibility.Hidden;
         


        }

      

        private void choixMusic(object sender, RoutedEventArgs e)
        {
            
            gridChoix.Children.Clear();
            gridContent.Children.Clear();
            gridImage.Children.Clear();
            gridChoix.Children.Add(listBoxChoix);
            listBoxChoix.Visibility = Visibility.Visible;


        }

        private void clicFragile(object sender, RoutedEventArgs e)
        {
            gridChoix.Children.Clear();
            gridContent.Children.Clear();
            Frame frame = new Frame();
            gridChoix.Children.Add(frame);
            Uri uri = new Uri("PageNIN.xaml", UriKind.Relative);
            frame.Navigate(new PageNIN());


        }

        private void clicDeadmau5(object sender, RoutedEventArgs e)
        {
            gridChoix.Children.Clear();
            gridContent.Children.Clear();

            Frame frame = new Frame();
            gridChoix.Children.Add(frame);
            
            frame.Navigate(new PageDeadmau5());

        }

        private void choixFilm(object sender, RoutedEventArgs e)
        {
            if (listBoxChoix.Visibility != Visibility.Hidden)
            {
                listBoxChoix.Visibility = Visibility.Hidden;
            }

            Random random = new Random();
            int rdm = random.Next(0, 5);
            gridChoix.Children.Clear();
            gridContent.Children.Clear();
            gridImage.Children.Clear();

        



            Image img = new Image();
            img.Width = 450;
            img.Height = 450;
            ImageSource src = new BitmapImage(new Uri((string)array[rdm]));
            img.Source = src;
            gridImage.Children.Add(img);



            

        }

        private void choixJeux(object sender, RoutedEventArgs e)
        {

            if (listBoxChoix.Visibility != Visibility.Hidden)
            {
                listBoxChoix.Visibility = Visibility.Hidden;
            }
            gridChoix.Children.Clear();
            gridContent.Children.Clear();
            gridImage.Children.Clear();
            gridContent.Children.Add(rect1);
            gridContent.Children.Add(cercle);
            rect1.Visibility = Visibility.Visible;
            cercle.Visibility = Visibility.Visible;
        }

      


    }



}
