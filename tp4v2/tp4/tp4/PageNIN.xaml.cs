﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace tp4
{
    /// <summary>
    /// Logique d'interaction pour PageNIN.xaml
    /// </summary>
    public partial class PageNIN : Page
    {

        private Hyperlink link;
        public PageNIN()
        {
            InitializeComponent();
            //les vidéos n'ont pas rapport, je n'ai pas pu trouver de videos/sons concernant la musique...
            mediaNIN.Source = new Uri(@"http://techslides.com/demos/sample-videos/small.mp4 ", UriKind.Absolute);
            
        }


        private void linkNIN(object sender, RoutedEventArgs e)
        {
            
            System.Diagnostics.Process.Start("chrome.exe","http://www.nin.com");

        }

        private void playVid2(object sender, RoutedEventArgs e)
        {
            mediaNIN.Play();

        }

        private void stopVid2(object sender, RoutedEventArgs e)
        {
            mediaNIN.Pause();

        }


        private void loop2(object sender, EventArgs e)
        {
            mediaNIN.Position = TimeSpan.Zero;
            mediaNIN.Play();
        }

    }
}
